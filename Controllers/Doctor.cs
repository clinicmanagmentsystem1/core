using System;
using System.Collections.Generic;
using System.IO;
using CORE.Models;
using Newtonsoft.Json;

namespace CORE.Controllers
{
    public class DoctorController
    {
        private string _filePath;
        private List<DoctorModel> _doctors;

        #region Constructors

        public DoctorController()
        {
            _filePath = "doctor.json";
            Load();
        }

        public DoctorController(String filePath)
        {
            _filePath = filePath;
            Load();
        }

        #endregion

        #region Create

        public void Create(DoctorModel doctor)
        {
            if (_doctors != null)
            {
                if (_doctors.Count > 0)
                {
                    doctor.Id = _doctors[_doctors.Count - 1].Id + 1;
                }
                else
                {
                    doctor.Id = 1;
                }
            }
            else
            {
                doctor.Id = 1;
                _doctors = new List<DoctorModel>();
            }

            _doctors.Add(doctor);
            Save();
        }

        #endregion

        #region Get

        public List<DoctorModel> GetAll()
        {
            return _doctors;
        }

        public DoctorModel GetById(int id)
        {
            return _doctors.Find(doctor => doctor.Id == id);
        }

        #endregion

        #region Remove

        public void Remove(int id)
        {
            _doctors.Remove(_doctors.Find(doctor => doctor.Id == id));
            
            Save();
        }

        #endregion
        
        #region SAVE-LOAD

        public void Save()
        {
            string json = JsonConvert.SerializeObject(_doctors);
            File.WriteAllText(_filePath, json);
        }

        public void Load()
        {
            if (!File.Exists(_filePath))
            {
                List<DoctorModel> doctors = new List<DoctorModel>();
                _doctors = doctors;
            }

            try
            {
                string json = File.ReadAllText(_filePath);
                List<DoctorModel> doctors = JsonConvert.DeserializeObject<List<DoctorModel>>(json);
                _doctors = doctors;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error loading doctors from file: {ex.Message}");
            }
        }

        #endregion
    }
}