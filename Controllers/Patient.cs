using System;
using System.Collections.Generic;
using System.IO;
using CORE.Models;
using Newtonsoft.Json;

namespace CORE.Controllers
{
    public class PatientController
    {
        private string _filePath;
        private List<PatientModel> _patient;

        #region Constructors

        public PatientController()
        {
            _filePath = "patient.json";
            Load();
        }

        public PatientController(String filePath)
        {
            _filePath = filePath;
            Load();
        }

        #endregion

        #region Create

        public void Create(PatientModel patient)
        {
            if (_patient != null)
            {
                if (_patient.Count > 0)
                {
                    patient.Id = _patient[_patient.Count - 1].Id + 1;
                }
                else
                {
                    patient.Id = 1;
                }
            }
            else
            {
                patient.Id = 1;
                _patient = new List<PatientModel>();
            }

            _patient.Add(patient);
            Save();
        }

        #endregion

        #region Get

        public List<PatientModel> GetAll()
        {
            return _patient;
        }

        public PatientModel GetById(int id)
        {
            return _patient.Find(patient => patient.Id == id);
        }

        #endregion

        #region Remove

        public void Remove(int id)
        {
            _patient.Remove(_patient.Find(doctor => doctor.Id == id));

            Save();
        }

        #endregion

        #region SAVE-LOAD

        public void Save()
        {
            string json = JsonConvert.SerializeObject(_patient);
            File.WriteAllText(_filePath, json);
        }

        public void Load()
        {
            if (!File.Exists(_filePath))
            {
                List<PatientModel> doctors = new List<PatientModel>();
                _patient = doctors;
            }

            try
            {
                string json = File.ReadAllText(_filePath);
                List<PatientModel> patients = JsonConvert.DeserializeObject<List<PatientModel>>(json);
                _patient = patients;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error loading patient from file: {ex.Message}");
            }
        }

        #endregion
    }
}